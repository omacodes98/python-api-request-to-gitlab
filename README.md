# API Request to GitLab 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

An Application that talks to an API of an external application(GitLab) and lists all the public GitLab repositories for a specified user 

## Technologies Used 

* Python 

* GitLab 

* Pycharm 

* Git 

## Steps 

Steps 1: Use pip to install request 

[images](/images/01_use_pip_to_install_request.png)

Step 2: Import request module 

[images](/images/02_import_request_module.png)

Step 3: Use request function get to make python send a request to talk to gitlab insert appropriate url

[images](/images/03_use_request_function_get_to_make_python_send_a_request_to_talk_to_gitlab_insert_apropriate_url.png)


Step 4: Save the request response into a variable 

[images](/images/04_save_the_request_response_into_a_variable.png)

Step 5: Print response and sert json to read the list of projects 

[images](/images/05_print_response_and_insert_json_to_read_the_list_of_projects.png)

Step 6: Save project list to a variable 

[images](/images/06_save_project_list_to_a_variable.png)

Step 7: Create a for loop to loop through dictionary name and web url 

[images](/images/07_create_a_for_loop_to_loop_through_dictionary_name_and_web_url.png)
## Installation

    brew install python 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/python-api-request-to-gitlab.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/python-api-request-to-gitlab

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.